## Build & Deployment
# Build image: docker build -t skycave .
# Tag image with repo name: docker tag skycave madskold/skycave:latest
# Push to private repo: docker push madskold/skycave:latest
# Validate restricted access to repo: docker rmi madskold/skycave:latest; docker logout; docker pull madskold/skycave:latest

## Execution
# Run container with defaults: docker run -d -p 7777:7777 --name mydaemon skycave
# Run container with custom commands: docker run -d -p 7777:7777 --name mydaemon skycave gradle daemon -Pcpf=load.cpf
# Run skycave client against cloud enviroment: docker run -d -v /root/.gradle --name myclient madskold/skycave:latest gradle cmd -Pcpf=operations.cpf

## Debug, Tracing & Testing
# Bash into container to validate correct file structure: docker run --rm -ti skycave bash
# Run tests and automatically remove container: docker run -ti --rm skycave gradle test jacocoRootReport

# Base ubuntu image containing java and gradle
FROM henrikbaerbak/jdk11-gradle68

# The author of this script
LABEL "author"="Mads Kold Sjørslev"
LABEL "author.email"="madskold@outlook.com"

# Set the working directory
WORKDIR /root/cave

# Copy cave source files to image
COPY build.gradle \
     gradle.properties \
     ReadMe.md \
     settings.gradle \
     /root/cave/

# DOC: Copy the dockerfile and associated helper scripts.
COPY Dockerfile /root/cave/

# Copy the individual sub projects.
COPY client/ client/
COPY common/ common/
COPY integration/ integration/
COPY server/ server/

# Expose port 7777
EXPOSE 7777

# Default command and arguments
CMD ["gradle", "daemon", "-Pcfp=http.cfp"]