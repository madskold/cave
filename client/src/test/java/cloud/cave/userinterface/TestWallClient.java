/*
 * Copyright (c) 2021. Henrik Bærbak Christensen, Aarhus University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package cloud.cave.userinterface;

import cloud.cave.domain.UpdateResult;
import org.junit.Before;
import org.junit.Test;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import java.util.List;

import cloud.cave.common.CommonClientCaveTest;
import cloud.cave.common.HelperMethods;
import cloud.cave.common.WallMessageDataTransferObject;
import cloud.cave.domain.Cave;
import cloud.cave.doubles.TestConstants;

import cloud.cave.domain.Player;

/**
 * Testing of the wall behavior on the client side.
 * 
 * @author Henrik Baerbak Christensen, Aarhus University
 *
 */

public class TestWallClient {

  private Player player;
  private Cave cave;

  @Before
  public void setup() {
    cave = CommonClientCaveTest.createCaveProxyForTesting();
    player = HelperMethods.loginPlayer(cave, TestConstants.MIKKEL_AARSKORT);
  }

  @Test
  public void shouldWriteToAndReadWall() {
    //Act
    player.addMessage("This is message no. 1");

    //Assert
    List<WallMessageDataTransferObject> wallContents = player.getMessageList(0);
    assertThat(wallContents.size(), is(1));
    assertThat(wallContents.get(0).getMessage(), containsString("This is message no. 1"));
  }

  @Test
  public void shouldHandleOutOfBoundsWallPages() {
    //Arrange
    player.addMessage("This is message no. 1");

    //Act
    List<WallMessageDataTransferObject> messageList = player.getMessageList(10);

    //Assert
    assertThat(messageList.size(), is(0));
  }

  @Test
  public void shouldEditWallMessage() {
    //Arrange
    player.addMessage("This is message no. 1");

    //Act
    List<WallMessageDataTransferObject> messageList = player.getMessageList(0);
    WallMessageDataTransferObject message = messageList.get(0);
    UpdateResult updateResult = player.updateMessage(message.getId(), "This is a updated message!");

    //Assert
    assertThat(updateResult, is(UpdateResult.UPDATE_OK));
    messageList = player.getMessageList(0);
    message = messageList.get(0);
    assertThat(message.getMessage(), containsString("This is a updated message!"));
  }
}