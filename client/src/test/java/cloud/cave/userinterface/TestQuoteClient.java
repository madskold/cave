/*
 * Copyright (c) 2021. Henrik Bærbak Christensen, Aarhus University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package cloud.cave.userinterface;

import org.junit.*;

import cloud.cave.common.CommonClientCaveTest;
import cloud.cave.common.HelperMethods;
import cloud.cave.domain.Cave;
import cloud.cave.domain.Player;
import cloud.cave.doubles.TestConstants;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;

/** Testing the quote method of Player on the client side.
 * 
 * @author Henrik Baerbak Christensen, Aarhus University
 *
 */
public class TestQuoteClient {
  private Cave cave;
  private Player player;

  @Before
  public void setUp() throws Exception {
    cave = CommonClientCaveTest.createCaveProxyForTesting();
    player = HelperMethods.loginPlayer(cave, TestConstants.MIKKEL_AARSKORT);
  }

  // TODO: Exercise - solve the 'quote-client' exercise
  @Test
  public void shouldGetQuoteOnClientSide() {


    // To solve this exercise, you have to implement the missing 'links' in
    // the broker chain of roles - proxy methods and invoker/dispatcher handling
    //assertThat(quote, containsString("NOT IMPLEMENTED YET"));
    String quote = player.getQuote(7);
    assertThat(quote, containsString("The true sign of intelligence is not knowledge but imagination. - Albert Einstein"));
  }

}
