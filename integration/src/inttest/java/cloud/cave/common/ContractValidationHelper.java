package cloud.cave.common;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.networknt.schema.JsonSchema;
import com.networknt.schema.JsonSchemaFactory;
import com.networknt.schema.SpecVersion;
import com.networknt.schema.ValidationMessage;

import java.io.*;
import java.util.Set;

public class ContractValidationHelper {

    public static boolean validateContract(String schemaName, String responseJson) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();

        // create an instance of the JsonSchemaFactory using version flag
        JsonSchemaFactory schemaFactory = JsonSchemaFactory.getInstance(SpecVersion.VersionFlag.V201909);

        try(
                InputStreamReader schemaStreamReader = getInputStreamReaderFor(schemaName)
        ){
            // read data from the stream and store it into JsonNode
            JsonNode json = objectMapper.readTree(responseJson);

            // get schema from the schemaStream and store it into JsonSchema
            JsonNode jsonSchema = objectMapper.readTree(schemaStreamReader);
            JsonSchema schema = schemaFactory.getSchema(jsonSchema);

            // create set of validation message and store result in it
            Set<ValidationMessage> validationResult = schema.validate( json );


            // show the validation errors
            if (!validationResult.isEmpty()) {

                // show all the validation error
                validationResult.forEach(vm -> System.out.println(vm.getMessage()));
            }

            return validationResult.isEmpty();
        }
    }

    private static InputStreamReader getInputStreamReaderFor(String cpfFileName)
            throws UnsupportedEncodingException {

        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        InputStream inputStream  = classLoader.getResourceAsStream(cpfFileName);

        return new InputStreamReader(inputStream, "UTF-8");
    }
}
