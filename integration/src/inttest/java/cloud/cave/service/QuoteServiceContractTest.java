package cloud.cave.service;

import cloud.cave.common.ContractValidationHelper;
import frds.broker.ipc.http.MimeMediaType;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.testcontainers.containers.GenericContainer;

import javax.servlet.http.HttpServletResponse;

import java.io.*;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class QuoteServiceContractTest {

    public static final int SERVER_PORT = 6777;

    @ClassRule
    public static GenericContainer quoteServiceContainer = new GenericContainer("henrikbaerbak/quote:msdo_1_0_1")
                    .withExposedPorts(SERVER_PORT);

    private static final HttpClient httpClient = HttpClient.newBuilder()
            .version(HttpClient.Version.HTTP_1_1)
            .connectTimeout(Duration.ofSeconds(10))
            .build();

    private String serverRootUrl;

    @Before
    public void setup()
    {
        String address = quoteServiceContainer.getContainerIpAddress();
        Integer port = quoteServiceContainer.getMappedPort(SERVER_PORT);
        serverRootUrl = "http://" + address + ":" + port;
    }

    @Test
    public void shouldGetQuoteHeader() throws InterruptedException, IOException {
        String quoteHeaderRoute = "/msdo/v1/quotes";
        String quoteHeaderSchema = "quote-header-schema.json";

        HttpRequest request = HttpRequest.newBuilder()
                .GET()
                .uri(URI.create(serverRootUrl + quoteHeaderRoute))
                .timeout(Duration.ofSeconds(10))
                .setHeader("Accept", MimeMediaType.TEXT_PLAIN)
                .setHeader("Content-Type", MimeMediaType.TEXT_PLAIN)
                .build();

        HttpResponse<String> response = httpClient.send(request,  HttpResponse.BodyHandlers.ofString());

        // Ensure that the server returns a succeeded response
        assertThat(response.statusCode(), is(HttpServletResponse.SC_OK));

        // Ensure that the response body correspond with the expected scheme
        assertThat(ContractValidationHelper.validateContract(quoteHeaderSchema, response.body()), is(true));
    }

    @Test
    public void shouldFailOnMalformedPathGetQuoteHeader() throws InterruptedException, IOException {
        String quoteHeaderRoute = "/msdo/v1/quote";

        HttpRequest request = HttpRequest.newBuilder()
                .GET()
                .uri(URI.create(serverRootUrl + quoteHeaderRoute))
                .timeout(Duration.ofSeconds(10))
                .setHeader("Accept", MimeMediaType.TEXT_PLAIN)
                .setHeader("Content-Type", MimeMediaType.TEXT_PLAIN)
                .build();

        HttpResponse<String> response = httpClient.send(request,  HttpResponse.BodyHandlers.ofString());

        // Ensure that the server returns a bad request on malformed path
        assertThat(response.statusCode(), is(HttpServletResponse.SC_BAD_REQUEST));
    }


    @Test
    public void shouldGetQuoteRecord() throws InterruptedException, IOException {
        String quoteHeaderRoute = "/msdo/v1/quotes/1";
        String quoteHeaderSchema = "quote-record-schema.json";

        HttpRequest request = HttpRequest.newBuilder()
                .GET()
                .uri(URI.create(serverRootUrl + quoteHeaderRoute))
                .timeout(Duration.ofSeconds(10))
                .setHeader("Accept", MimeMediaType.TEXT_PLAIN)
                .setHeader("Content-Type", MimeMediaType.TEXT_PLAIN)
                .build();

        HttpResponse<String> response = httpClient.send(request,  HttpResponse.BodyHandlers.ofString());

        // Ensure that the server returns a succeeded response
        assertThat(response.statusCode(), is(HttpServletResponse.SC_OK));

        // Ensure that the response body correspond with the expected scheme
        assertThat(ContractValidationHelper.validateContract(quoteHeaderSchema, response.body()), is(true));
    }

    @Test
    public void shouldFailOnMalformedIndexGetQuoteRecord() throws InterruptedException, IOException {
        String quoteHeaderRoute = "/msdo/v1/quotes/one";

        HttpRequest request = HttpRequest.newBuilder()
                .GET()
                .uri(URI.create(serverRootUrl + quoteHeaderRoute))
                .timeout(Duration.ofSeconds(10))
                .setHeader("Accept", MimeMediaType.TEXT_PLAIN)
                .setHeader("Content-Type", MimeMediaType.TEXT_PLAIN)
                .build();

        HttpResponse<String> response = httpClient.send(request,  HttpResponse.BodyHandlers.ofString());

        // Ensure that the server returns a bad request on malformed path
        assertThat(response.statusCode(), is(HttpServletResponse.SC_BAD_REQUEST));
    }

    @Test
    public void shouldFailOnIndex0GetQuoteRecord() throws InterruptedException, IOException {
        String quoteHeaderRoute = "/msdo/v1/quotes/0";

        HttpRequest request = HttpRequest.newBuilder()
                .GET()
                .uri(URI.create(serverRootUrl + quoteHeaderRoute))
                .timeout(Duration.ofSeconds(10))
                .setHeader("Accept", MimeMediaType.TEXT_PLAIN)
                .setHeader("Content-Type", MimeMediaType.TEXT_PLAIN)
                .build();

        HttpResponse<String> response = httpClient.send(request,  HttpResponse.BodyHandlers.ofString());

        // Ensure that the server returns a bad request on malformed path
        assertThat(response.statusCode(), is(HttpServletResponse.SC_NOT_FOUND));
    }

    @Test
    public void shouldFailOnIndexMaxIntGetQuoteRecord() throws InterruptedException, IOException {
        String quoteHeaderRoute = "/msdo/v1/quotes/2147483647";

        HttpRequest request = HttpRequest.newBuilder()
                .GET()
                .uri(URI.create(serverRootUrl + quoteHeaderRoute))
                .timeout(Duration.ofSeconds(10))
                .setHeader("Accept", MimeMediaType.TEXT_PLAIN)
                .setHeader("Content-Type", MimeMediaType.TEXT_PLAIN)
                .build();

        HttpResponse<String> response = httpClient.send(request,  HttpResponse.BodyHandlers.ofString());

        // Ensure that the server returns a bad request on malformed path
        assertThat(response.statusCode(), is(HttpServletResponse.SC_NOT_FOUND));
    }
}
