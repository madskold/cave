package cloud.cave.service;

import cloud.cave.common.ServerConfiguration;
import org.junit.*;
import org.testcontainers.containers.GenericContainer;


import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

public class RedisCaveStorageConnectorTest extends TestCaveStorageBase {
    public static final int SERVER_PORT = 6379;

    @Rule
    public GenericContainer redisContainer =
            new GenericContainer("redis:6.2.5-alpine")
                    .withExposedPorts(SERVER_PORT);
    private String address;
    private Integer port;

    @Before
    public void setUp() throws Exception {
        address = redisContainer.getContainerIpAddress();
        port = redisContainer.getMappedPort(SERVER_PORT);

        storage = new RedisCaveStorage();
        storage.initialize(null, new ServerConfiguration(address, port));
    }

    @After
    public void tearDown() {
        storage.disconnect();
    }

    @Test
    public void shouldIncreaseCoverageForCaveStorage() {
        String t = storage.toString();
        assertThat(t, is("RedisCaveStorage (5 rooms. 0 players)"));
        ServerConfiguration cfg = storage.getConfiguration();
        assertThat(cfg.get(0).getHostName(), is(address));
        assertThat(cfg.get(0).getPortNumber(), is(port));
    }
}
