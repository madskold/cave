package cloud.cave.service;

import cloud.cave.common.ServerConfiguration;
import cloud.cave.service.quote.QuoteHeader;
import cloud.cave.service.quote.QuoteRecord;
import cloud.cave.service.quote.QuoteService;
import cloud.cave.service.quote.RealQuoteService;

import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.testcontainers.containers.GenericContainer;

import javax.servlet.http.HttpServletResponse;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;


public class QuoteServiceConnectorTest {
    public static final int SERVER_PORT = 6777;

    @ClassRule
    public static GenericContainer quoteServiceContainer = new GenericContainer("henrikbaerbak/quote:msdo_1_0_1")
            .withExposedPorts(SERVER_PORT);

    private QuoteService quoteService;

    @Before
    public void setup()
    {
        String address = quoteServiceContainer.getContainerIpAddress();
        Integer port = quoteServiceContainer.getMappedPort(SERVER_PORT);

        quoteService = new RealQuoteService();
        quoteService.initialize(null, new ServerConfiguration(address, port));
    }


    @Test()
    public void ShouldGetQuoteWithIndex1() {
        QuoteRecord quote = quoteService.getQuote(1);
        assertThat(quote.getStatusCode(), is(HttpServletResponse.SC_OK));
    }

    @Test()
    public void ShouldGetQuoteWithIndex0() {
        QuoteRecord quote = quoteService.getQuote(0);
        assertThat(quote.getStatusCode(), is(HttpServletResponse.SC_NOT_FOUND));
    }

    @Test()
    public void ShouldGetQuoteHeader() {
        QuoteHeader quoteHeader = quoteService.getHeader();
        assertThat(quoteHeader.totalItems(), greaterThan(0));
    }
}
