package cloud.cave.service.subscription;

import cloud.cave.common.ServerConfiguration;
import cloud.cave.common.ServerData;
import cloud.cave.config.ObjectManager;
import cloud.cave.server.common.SubscriptionRecord;
import cloud.cave.service.SubscriptionService;
import com.google.gson.Gson;
import frds.broker.ipc.http.MimeMediaType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.util.Base64;

public class RealSubscriptionService implements SubscriptionService {

    private ServerConfiguration configuration;
    private Gson gson;
    private Logger logger;

    private static final HttpClient httpClient = HttpClient.newBuilder()
            .connectTimeout(Duration.ofSeconds(10))
            .build();

    public RealSubscriptionService(){
        this.gson = new Gson();

        // Create the logging
        this.logger = LoggerFactory.getLogger(RealSubscriptionService.class);
    }

    @Override
    public void initialize(ObjectManager objectManager, ServerConfiguration config) {

        this.configuration = config;
    }

    @Override
    public void disconnect() {

    }

    @Override
    public ServerConfiguration getConfiguration() {
        return this.configuration;
    }

    @Override
    public SubscriptionRecord authorize(String loginName, String password) {
        String baseURL = getBaseUrl();
        String authorizeRoute = "/api/v3/authorize";

        String basicAuth = "skycave_daemon:f8tqv56utx";
        String encodedBasicAuth = Base64.getEncoder().encodeToString(basicAuth.getBytes());

        AuthorizeRequestDto authorizeRequestDto = new AuthorizeRequestDto(loginName, password);
        String payload = gson.toJson(authorizeRequestDto);

        HttpRequest request = HttpRequest.newBuilder()
                .POST(HttpRequest.BodyPublishers.ofString(payload))
                .uri(URI.create(baseURL + authorizeRoute))
                .setHeader("Accept", MimeMediaType.APPLICATION_JSON) // add request header
                .setHeader("Authorization", "Basic " + encodedBasicAuth)
                .timeout(Duration.ofSeconds(10))
                .build();

        HttpResponse<String> response = null;

        try {
            response = httpClient.send(request,  HttpResponse.BodyHandlers.ofString());
        } catch (IOException e) {
            this.logger.error(e.getMessage(), e);
        } catch (InterruptedException e) {
            this.logger.error(e.getMessage(), e);
        }

        if (response == null) {
            return new SubscriptionRecord(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }

        int statusCode = response.statusCode();

        if(statusCode != HttpServletResponse.SC_OK){
            return new SubscriptionRecord(statusCode);
        }

        String body = response.body();
        AuthorizeResult authorizeResult = gson.fromJson(body, AuthorizeResult.class);

        return MapToSubscriptionRecord(authorizeResult);
    }

    private SubscriptionRecord MapToSubscriptionRecord(AuthorizeResult authorizeResult){

        AuthorizeResult.AuthorizeResultSubscription authorizeResultSubscription = authorizeResult.subscription;

        SubscriptionRecord subscriptionRecord = new SubscriptionRecord(
                authorizeResultSubscription.getPlayerID(),
                authorizeResultSubscription.getPlayerName(),
                authorizeResultSubscription.getGroupName(),
                authorizeResultSubscription.getRegion()
                );

        subscriptionRecord.setAccessToken(authorizeResult.accessToken);

        return subscriptionRecord;
    }

    private String getBaseUrl() {
        ServerData serverData = this.configuration.get(0);
        String baseURL = "https://" + serverData.getHostName() + ":" + serverData.getPortNumber();
        return baseURL;
    }
}
