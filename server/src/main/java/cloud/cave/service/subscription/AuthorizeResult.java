package cloud.cave.service.subscription;

import cloud.cave.domain.Region;

public class AuthorizeResult {
    public AuthorizeResultSubscription subscription;
    public String message;
    public String accessToken;
    public int httpStatusCode;

    public class AuthorizeResultSubscription{
        private String groupName;
        private String dateCreated;
        private String playerName;
        private String loginName;
        private Region region;
        private String groupToken;
        private String playerID;

        public String getGroupName() {
            return groupName;
        }

        public String getDateCreated() {
            return dateCreated;
        }

        public String getPlayerName() {
            return playerName;
        }

        public String getLoginName() {
            return loginName;
        }

        public Region getRegion() {
            return region;
        }

        public String getGroupToken() {
            return groupToken;
        }

        public String getPlayerID() {
            return playerID;
        }
    }
}

