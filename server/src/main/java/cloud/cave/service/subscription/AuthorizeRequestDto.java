package cloud.cave.service.subscription;

public class AuthorizeRequestDto {
    private String loginName;
    private String password;

    public AuthorizeRequestDto(String loginName, String password){
        this.loginName = loginName;
        this.password = password;
    }

    public String getLoginName() {
        return loginName;
    }

    public String getPassword() {
        return password;
    }
}
