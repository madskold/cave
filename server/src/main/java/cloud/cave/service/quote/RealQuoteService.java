package cloud.cave.service.quote;

import cloud.cave.common.ServerConfiguration;
import cloud.cave.common.ServerData;
import cloud.cave.config.ObjectManager;
import com.google.gson.Gson;
import frds.broker.*;
import frds.broker.ipc.http.MimeMediaType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.util.concurrent.ThreadLocalRandom;


public class RealQuoteService implements QuoteService, ClientProxy {

    private ServerConfiguration configuration;
    private Gson gson;
    private Logger logger;

    private static final HttpClient httpClient = HttpClient.newBuilder()
            .version(HttpClient.Version.HTTP_1_1)
            .connectTimeout(Duration.ofSeconds(10))
            .build();


    public RealQuoteService() {
        this.gson = new Gson();

        // Create the logging
        this.logger = LoggerFactory.getLogger(RealQuoteService.class);
    }

    @Override
    public void initialize(ObjectManager objectManager, ServerConfiguration config) {
        this.configuration = config;
    }

    @Override
    public void disconnect() {

    }

    @Override
    public ServerConfiguration getConfiguration() {
        return this.configuration;
    }

    @Override
    public QuoteRecord getQuote(int quoteIndex) {
        String baseURL = getBaseUrl();
        String quotesRoute = "/msdo/v1/quotes/";

        HttpRequest request = HttpRequest.newBuilder()
                .GET()
                .uri(URI.create(baseURL + quotesRoute + quoteIndex))
                .timeout(Duration.ofSeconds(10))
                .setHeader("Accept", MimeMediaType.TEXT_PLAIN)
                .setHeader("Content-Type", MimeMediaType.TEXT_PLAIN)
                .build();

        HttpResponse<String> response = null;

        try {
            response = httpClient.send(request,  HttpResponse.BodyHandlers.ofString());
        } catch (IOException e) {
            this.logger.error(e.getMessage(), e);
        } catch (InterruptedException e) {
            this.logger.error(e.getMessage(), e);
        }

        if (response == null){
            return createFaultQuoteRecord(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }

        int statusCode = response.statusCode();
        if(statusCode != HttpServletResponse.SC_OK){
            return createFaultQuoteRecord(statusCode);
        }

        String body = response.body();

        QuoteRecordDto quoteDto = gson.fromJson(body, QuoteRecordDto.class);

        return new QuoteRecord(quoteDto.getNumber(), quoteDto.getQuote(), quoteDto.getAuthor(), statusCode);
    }

    private String getBaseUrl() {
        ServerData serverData = this.configuration.get(0);
        String baseURL = "http://" + serverData.getHostName() + ":" + serverData.getPortNumber();
        return baseURL;
    }

    private QuoteRecord createFaultQuoteRecord (int statusCode){
        return new QuoteRecord(0, "", "", statusCode);
    }

    @Override
    public QuoteHeader getHeader() {
        String baseURL = getBaseUrl();
        String quoteHeaderRoute = "/msdo/v1/quotes";

        HttpRequest request = HttpRequest.newBuilder()
                .GET()
                .uri(URI.create(baseURL + quoteHeaderRoute))
                .timeout(Duration.ofSeconds(10))
                .setHeader("Accept", MimeMediaType.TEXT_PLAIN)
                .setHeader("Content-Type", MimeMediaType.TEXT_PLAIN)
                .build();

        HttpResponse<String> response = null;

        try {
            response = httpClient.send(request,  HttpResponse.BodyHandlers.ofString());
        } catch (IOException e) {
            this.logger.error(e.getMessage(), e);
        } catch (InterruptedException e) {
            this.logger.error(e.getMessage(), e);
        }

        if (response == null){
            return createFaultQuoteHeader();
        }

        int statusCode = response.statusCode();
        if(statusCode != HttpServletResponse.SC_OK){
            return createFaultQuoteHeader();
        }

        String body = response.body();

        QuoteHeader quoteHeader = gson.fromJson(body, QuoteHeader.class);

        return quoteHeader;
    }

    private QuoteHeader createFaultQuoteHeader (){
        return new QuoteHeader("", "", "", null, 0);
    }

}
