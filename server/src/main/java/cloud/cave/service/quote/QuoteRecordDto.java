package cloud.cave.service.quote;

public class QuoteRecordDto {
    private final String quote;
    private final String author;
    private final int number;

    public QuoteRecordDto(int number, String quote, String author) {
        this.number = number;
        this.quote = quote;
        this.author = author;
    }

    public String getQuote() {
        return quote;
    }

    public String getAuthor() {
        return author;
    }

    public int getNumber() {
        return number;
    }
}
