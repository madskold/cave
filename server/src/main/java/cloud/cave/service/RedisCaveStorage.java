package cloud.cave.service;

import cloud.cave.common.ServerConfiguration;
import cloud.cave.common.ServerData;
import cloud.cave.config.ObjectManager;
import cloud.cave.domain.Direction;
import cloud.cave.server.common.*;
import cloud.cave.service.wall.MessageRecord;
import com.google.gson.Gson;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import javax.servlet.http.HttpServletResponse;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class RedisCaveStorage  implements CaveStorage {

    public static final String ROOMS = "rooms";
    public static final String PLAYERS = "players";
    public static final String PLAYERS_IN_ROOMS = "playersInRooms";
    public static final String MESSAGES = "messages";
    public static final String MESSAGES_IN_ROOMS = "messagesInRooms";

    private ServerConfiguration configuration;
    private Jedis jedis;
    private final Gson gson;

    // Strategy to delegate how to define timestamps
    private NowStrategy nowStrategy;

    public RedisCaveStorage() {

        gson = new Gson();

        // Default to a timestamp strategy that uses the real clock.
        nowStrategy = new RealNowStrategy();
    }

    @Override
    public void initialize(ObjectManager objectManager, ServerConfiguration config) {
        this.configuration = config;
        ServerData serverData = this.configuration.get(0);
        JedisPool pool = new JedisPool(serverData.getHostName(), serverData.getPortNumber());
        jedis = pool.getResource();

        // Initialize the default room layout
        RoomRecord entryRoom = new RoomRecord(
                "You are standing at the end of a road before a small brick building.",
                WILL_CROWTHER_ID);
        this.addRoom(new Point3(0, 0, 0).getPositionString(), entryRoom);
        this.addRoom(new Point3(0, 1, 0).getPositionString(), new RoomRecord(
                "You are in open forest, with a deep valley to one side.", WILL_CROWTHER_ID));
        this.addRoom(new Point3(1, 0, 0).getPositionString(), new RoomRecord(
                "You are inside a building, a well house for a large spring.", WILL_CROWTHER_ID));
        this.addRoom(new Point3(-1, 0, 0).getPositionString(), new RoomRecord(
                "You have walked up a hill, still in the forest.", WILL_CROWTHER_ID));
        this.addRoom(new Point3(0, 0, 1).getPositionString(), new RoomRecord(
                "You are in the top of a tall tree, at the end of a road.", WILL_CROWTHER_ID));
    }

    @Override
    public void disconnect() {

    }

    @Override
    public ServerConfiguration getConfiguration() {
        return this.configuration;
    }


    // Rooms

    @Override
    public RoomRecord getRoom(String positionString) {
        String asJSON = jedis.hget(ROOMS, positionString);
        if (asJSON == null) return null;
        return gson.fromJson(asJSON, RoomRecord.class);
    }

    @Override
    public int addRoom(String positionString, RoomRecord newRoom) {
        String asJSON = gson.toJson(newRoom);

        // if there is already a room, return FORBIDDEN
        if ( jedis.hexists(ROOMS, positionString) ) {
            return HttpServletResponse.SC_FORBIDDEN;
        }

        // Simulate classic DB behaviour: timestamp record and
        // assign unique id
        RoomRecord recordInDB = new RoomRecord(newRoom);
        ZonedDateTime now = nowStrategy.now();
        recordInDB.setCreationTime(now);
        recordInDB.setId(UUID.randomUUID().toString());

        jedis.hset(ROOMS, positionString, asJSON);
        return HttpServletResponse.SC_CREATED;
    }

    @Override
    public int updateRoom(String positionString, RoomRecord updatedRoom) {
        String asJSON = gson.toJson(updatedRoom);

        if(!jedis.hexists(ROOMS, positionString)){
            return HttpServletResponse.SC_NOT_FOUND;
        }

        RoomRecord presentRecord = gson.fromJson(jedis.hget(ROOMS, positionString), RoomRecord.class);
        if (! presentRecord.getCreatorId().equals(updatedRoom.getCreatorId())) {
            return HttpServletResponse.SC_UNAUTHORIZED;
        }

        jedis.hset(ROOMS, positionString, asJSON);
        return HttpServletResponse.SC_OK;
    }

    @Override
    public List<Direction> getSetOfExitsFromRoom(String positionString) {
        Map<String, String> rooms = jedis.hgetAll(ROOMS);

        List<Direction> listOfExits = new ArrayList<Direction>();
        Point3 pZero = Point3.parseString(positionString);
        Point3 p;
        for ( Direction d : Direction.values()) {
            p = new Point3(pZero.x(), pZero.y(), pZero.z());
            p.translate(d);
            String position = p.getPositionString();
            if (rooms.containsKey(position)){
                listOfExits.add(d);
            }
        }
        return listOfExits;
    }


    // Palyers

    @Override
    public PlayerRecord getPlayerByID(String playerID) {
        String playerJson = jedis.hget(PLAYERS, playerID);
        if (playerJson == null) return null;
        PlayerRecord player = gson.fromJson(playerJson, PlayerRecord.class);
        return player;
    }

    @Override
    public void updatePlayerRecord(PlayerRecord record) {
        String asJSON = gson.toJson(record);
        jedis.hset(PLAYERS, record.getPlayerID(), asJSON);
        jedis.hset(PLAYERS_IN_ROOMS, record.getPlayerID(), record.getPositionAsString());
    }

    @Override
    public List<PlayerRecord> computeListOfPlayersAt(String positionString) {
        List<PlayerRecord> theList = new ArrayList<PlayerRecord>();
        Map<String, String> playerInRooms = jedis.hgetAll(PLAYERS_IN_ROOMS);

        for (String playerId : playerInRooms.keySet()) {
            String roomId = playerInRooms.get(playerId);
            if (roomId.equals(positionString)){
                PlayerRecord player = this.getPlayerByID(playerId);
                if (player.isInCave()){
                    theList.add(player);
                }
            }
        }

        return theList;
    }

    // Wall messages

    @Override
    public void addMessage(String positionInCave, MessageRecord messageRecord) {

        // Simulate 'classic DB' behaviour, assign unique
        // id to item and timestamp it
        MessageRecord newRecord = new MessageRecord(messageRecord);
        ZonedDateTime now = nowStrategy.now();
        newRecord.setCreatorTimeStampISO8601(now);
        newRecord.setId(UUID.randomUUID().toString());

        String messageJson = gson.toJson(newRecord);
        jedis.hset(MESSAGES, getRoomMessageKey(positionInCave, newRecord.getId()), messageJson);
        jedis.hset(MESSAGES_IN_ROOMS, getRoomMessageKey(positionInCave, newRecord.getId()), positionInCave);
    }

    private String getRoomMessageKey(String positionInCave, String messageId) {
        return positionInCave + "_" + messageId;
    }

    private String getMessageId(String messageRoomId) {
        return messageRoomId.split("_")[1];
    }

    @Override
    public int updateMessage(String positionInCave, String messageId, MessageRecord newMessageRecord) {

        String messageJson = jedis.hget(MESSAGES, getRoomMessageKey(positionInCave, messageId));
        String roomId = jedis.hget(MESSAGES_IN_ROOMS, getRoomMessageKey(positionInCave, messageId));

        // Check if message exists in database
        if (messageJson == null || roomId == null) {
            return HttpServletResponse.SC_NOT_FOUND;
        }

        // Check if the requested message is located in the requested room
        if (roomId == positionInCave) {
            return HttpServletResponse.SC_NOT_FOUND;
        }

        // Check if the requested message is located in the requested room
        if (roomId == positionInCave) {
            return HttpServletResponse.SC_NOT_FOUND;
        }

        MessageRecord currentMessageRecord = gson.fromJson(messageJson, MessageRecord.class);

        // Bail out if the found message was not created by same person
        if (!currentMessageRecord.getCreatorId().equals(newMessageRecord.getCreatorId())) {
            return HttpServletResponse.SC_UNAUTHORIZED;
        }

        // Update message and enter it back into the list
        MessageRecord updatedOne = new MessageRecord(currentMessageRecord);
        updatedOne.setContents(newMessageRecord.getContents());

        String updatedMessageJson = gson.toJson(updatedOne);
        jedis.hset(MESSAGES, getRoomMessageKey(positionInCave, messageId), updatedMessageJson);

        return HttpServletResponse.SC_OK;
    }

    @Override
    public List<MessageRecord> getMessageList(String positionInCave, int startIndex, int pageSize) {
        List<MessageRecord> theList = new ArrayList<MessageRecord>();
        Map<String, String> messagesInRooms = jedis.hgetAll(MESSAGES_IN_ROOMS);

        for (String messageRoomId : messagesInRooms.keySet()) {
            String roomId = messagesInRooms.get(messageRoomId);
            if (roomId.equals(positionInCave)){
                String messageId = this.getMessageId(messageRoomId);

                String messageJson = jedis.hget(MESSAGES, messageId);
                if (messageJson == null)
                    continue;

                MessageRecord message = gson.fromJson(messageJson, MessageRecord.class);
                theList.add(message);
            }
        }

        return theList;
    }

    public String toString() {
        Map<String, String> players = jedis.hgetAll(PLAYERS);
        Map<String, String> rooms = jedis.hgetAll(ROOMS);

        return "RedisCaveStorage (" + rooms.keySet().size() + " rooms. " +
                players.keySet().size() + " players)";
    }
}
