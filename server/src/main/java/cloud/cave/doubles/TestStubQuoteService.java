package cloud.cave.doubles;

import cloud.cave.common.ServerConfiguration;
import cloud.cave.config.ObjectManager;
import cloud.cave.server.common.NowStrategy;
import cloud.cave.server.common.RealNowStrategy;
import cloud.cave.service.quote.QuoteHeader;
import cloud.cave.service.quote.QuoteRecord;
import cloud.cave.service.quote.QuoteService;

import javax.servlet.http.HttpServletResponse;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/*
* Short argumentation for which type the test double is (stub, mock, fake, spy, ...):
* Jeg vil betragte denne implementation som en stub, da den kun benyttes som inderekte data.
* */

public class TestStubQuoteService implements QuoteService {
    private ServerConfiguration config;
    private NowStrategy nowStrategy;

    private Map<Integer, QuoteRecord> quoteRecords;
    private static final int DefaultQuoteIndex = 1;

    public TestStubQuoteService() {
        quoteRecords = new HashMap<>();

        nowStrategy = new RealNowStrategy();
    }

    @Override
    public void initialize(ObjectManager objectManager, ServerConfiguration config) {

        quoteRecords.put(1, new QuoteRecord(1,
                "Take small steps - use the ladder, not the vaulting pole.",
                "Henrik Bærbak Christensen",
                HttpServletResponse.SC_OK));

        quoteRecords.put(7, new QuoteRecord(7,
                "The true sign of intelligence is not knowledge but imagination.",
                "Albert Einstein",
                HttpServletResponse.SC_OK));

        quoteRecords.put(13, new QuoteRecord(13,
                "Education is what remains after one has forgotten what one has learned in school.",
                "Albert Einstein",
                HttpServletResponse.SC_OK));
    }

    @Override
    public void disconnect() {

    }

    @Override
    public ServerConfiguration getConfiguration() {
        return config;
    }

    @Override
    public QuoteRecord getQuote(int quoteIndex) {
        if (quoteIndex >= 15) {
            return new QuoteRecord(quoteIndex,
                    "",
                    "",
                    HttpServletResponse.SC_NOT_FOUND);
        }

        QuoteRecord quote = quoteRecords.getOrDefault(quoteIndex, null);

        if (quote == null) {
            quote = quoteRecords.getOrDefault(DefaultQuoteIndex, null);
        }

        return quote;
    }

    //TODO: Is not done
    @Override
    public QuoteHeader getHeader() {
        ZonedDateTime now = nowStrategy.now();

        return new QuoteHeader(GetTimeAsISO8601String(now),
                null,
                "TestStubQuoteService",
                GetQuoteAuthors(),
                GetTotalCount());
    }

    private String GetTimeAsISO8601String(ZonedDateTime timeStamp) {
        return timeStamp.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME);
    }

    private List<String> GetQuoteAuthors() {
        return quoteRecords
                .values()
                .stream()
                .map(QuoteRecord::getAuthor)
                .distinct()
                .collect(Collectors.toList());
    }

    private Integer GetTotalCount() {
        return quoteRecords.size();
    }
}
