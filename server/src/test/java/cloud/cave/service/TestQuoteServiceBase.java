package cloud.cave.service;

import cloud.cave.service.quote.QuoteHeader;
import cloud.cave.service.quote.QuoteRecord;
import cloud.cave.service.quote.QuoteService;
import org.junit.Test;

import javax.servlet.http.HttpServletResponse;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public abstract class TestQuoteServiceBase {

    protected QuoteService quoteService;

    @Test()
    public void ShouldGetQuoteWithIndex7() {
        QuoteRecord quote = quoteService.getQuote(7);

        assertThat(quote.getQuote(), is("The true sign of intelligence is not knowledge but imagination."));
        assertThat(quote.getAuthor(), is("Albert Einstein"));
        assertThat(quote.getNumber(), is(7));
        assertThat(quote.getStatusCode(), is(HttpServletResponse.SC_OK));
    }

    @Test()
    public void ShouldGetQuoteWithIndex13() {
        QuoteRecord quote = quoteService.getQuote(13);

        assertThat(quote.getQuote(), is("Education is what remains after one has forgotten what one has learned in school."));
        assertThat(quote.getAuthor(), is("Albert Einstein"));
        assertThat(quote.getNumber(), is(13));
        assertThat(quote.getStatusCode(), is(HttpServletResponse.SC_OK));
    }

    @Test()
    public void ShouldGetDefaultQuoteWithNonMatchingIndex() {
        QuoteRecord quote = quoteService.getQuote(5);

        assertThat(quote.getQuote(), is("Take small steps - use the ladder, not the vaulting pole."));
        assertThat(quote.getAuthor(), is("Henrik Bærbak Christensen"));
        assertThat(quote.getNumber(), is(1));
        assertThat(quote.getStatusCode(), is(HttpServletResponse.SC_OK));
    }

    @Test()
    public void ShouldGetNotFound404QuoteWithIndexOver14() {
        //Boundary test values
        QuoteRecord quote = quoteService.getQuote(14);

        assertThat(quote.getNumber(), is(1));
        assertThat(quote.getStatusCode(), is(HttpServletResponse.SC_OK));


        quote = quoteService.getQuote(15);

        assertThat(quote.getNumber(), is(15));
        assertThat(quote.getStatusCode(), is(HttpServletResponse.SC_NOT_FOUND));
    }

    @Test()
    public void ShouldGetQuoteHeader() {
        QuoteHeader quoteHeader = quoteService.getHeader();

        assertThat(quoteHeader.totalItems(), is(3));

        assertThat(quoteHeader.getAuthors().size(), is(2));
        assertThat(quoteHeader.getAuthors(), hasItem("Henrik Bærbak Christensen"));
        assertThat(quoteHeader.getAuthors(), hasItem("Albert Einstein"));
        // TODO: Test for the rest of the methods
    }
}
