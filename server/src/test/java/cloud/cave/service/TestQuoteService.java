/*
 * Copyright (c) 2021. Henrik Bærbak Christensen, Aarhus University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package cloud.cave.service;

import cloud.cave.common.CommonCaveTests;
import cloud.cave.config.ObjectManager;
import cloud.cave.service.quote.QuoteHeader;
import cloud.cave.service.quote.QuoteRecord;
import cloud.cave.service.quote.QuoteService;

import org.apache.http.MethodNotSupportedException;
import org.hamcrest.core.Is;
import org.junit.Before;
import org.junit.Test;

import javax.servlet.http.HttpServletResponse;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

/** Template for Test-Driven Development of the
 * QuoteService
 *
 */
public class TestQuoteService extends TestQuoteServiceBase {

  private ObjectManager objMgr;

  @Before
  public void setup() {
    objMgr = CommonCaveTests.createTestDoubledConfiguredCave();
    quoteService = objMgr.getQuoteService();
  }
}
