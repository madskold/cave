/*
 * Copyright (c) 2021. Henrik Bærbak Christensen, Aarhus University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package cloud.cave.service;

import org.junit.*;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.*;

import java.util.*;

import cloud.cave.common.ServerConfiguration;

import cloud.cave.domain.*;
import cloud.cave.doubles.*;
import cloud.cave.server.common.*;

import javax.servlet.http.HttpServletResponse;


/** TDD of (most of) the CaveStorage interface and
 * driving the FakeObject implementation.
 *
 * Some test cases are grouped in other test classes
 * as they serve for exercises.
 * 
 * @author Henrik Baerbak Christensen, Aarhus University.
 *
 */
public class TestCaveStorage extends TestCaveStorageBase {

  @Before
  public void setUp() throws Exception {
    storage = new FakeCaveStorage();
    storage.initialize(null, null);
  }
  
  @After
  public void tearDown() {
    storage.disconnect();
  }

  @Test
  public void shouldIncreaseCoverageForCaveStorage() {
    String t = storage.toString();
    assertThat(t, is("FakeCaveStorage (5 rooms. 0 players)"));
    ServerConfiguration cfg = storage.getConfiguration();
    assertThat(cfg, is(nullValue()));
  }
}
